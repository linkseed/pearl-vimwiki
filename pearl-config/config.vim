
" This will look for directories containing 'pack/*/start'
set packpath+=${PEARL_PKGVARDIR}

" Default settings
set nocompatible
filetype plugin on
syntax on
